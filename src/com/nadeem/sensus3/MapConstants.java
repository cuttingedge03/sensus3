package com.nadeem.sensus3;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public interface MapConstants {
	static final CameraPosition INDIRANAGAR_position = new CameraPosition.Builder()
	.target(new LatLng(12.973283, 77.641056)).zoom(17f).bearing(190)
	.build();
}
