package com.nadeem.sensus3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends ActionBarActivity implements Constants,
		MapConstants, OnMapClickListener, OnMapLongClickListener {
	private GoogleMap mMap;
	Database db;
	protected LocationManager locationManager;
	protected String currentLatitude, currentLongitude, id;
	private LocationListener ll;
	public static String latitude;
	public static String longitude;
	double lat, lon;
	String locname;
	double min;
	int minNodePos;
	ArrayList<Integer> selectedNodes;
	String unmatched;
	SharedPreferences prefs;
	LatLng destination, matchedSource, matchedDestination;
	Node n[];
	private String sourceOnCurve, destinationOnCurve;
	MenuItem item;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		db = new Database(this);
		setUpMapIfNeeded();
		// Cursor c;
		// c = db.getReadableDatabase().query(TABLE_CURVE5, null, null, null,
		// null, null, null);
		// c.moveToLast();
		// Cursor c1;
		// c1 = db.getReadableDatabase().query(TABLE_NODE, null, null, null,
		// null,
		// null, null);
		// c1.moveToLast();
		// db.getWritableDatabase().delete(TABLE_CURVE5,
		// "_id = " + c.getInt(c.getColumnIndex("_id")), null);
		// db.getWritableDatabase().delete(TABLE_NODE,
		// "_id = " + c1.getInt(c1.getColumnIndex("_id")), null);
		// db.getWritableDatabase().execSQL(
		// "DROP TABLE IF EXISTS " + TABLE_CURVE34);
		// db.getWritableDatabase().execSQL(
		// "CREATE TABLE " + TABLE_NODE1
		// + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
		// + LATITUDE + " REAL, " + LONGITUDE + " REAL);");
		// Cursor c;
		// c = db.getReadableDatabase().query(TABLE_CURVE5, null, null, null,
		// null, null, null);
		// c.moveToLast();
		// int id = c.getInt(c.getColumnIndex("_id"));
		// Cursor c1;
		// c1 = db.getReadableDatabase().query(TABLE_CURVE4, null, null, null,
		// null, null, null);
		// c1.moveToFirst();
		// // ContentValues value = new ContentValues();
		//
		// value.put(LATITUDE, c1.getDouble(c1.getColumnIndex(LATITUDE)));
		// value.put(LONGITUDE, c1.getDouble(c1.getColumnIndex(LONGITUDE)));
		// db.getWritableDatabase().update(TABLE_CURVE5, value,
		// "_id " + "= " + id, null);
		// Cursor c2;
		// c2 = db.getReadableDatabase().query(TABLE_CURVE11, null, null, null,
		// null, null, null);
		// c2.moveToFirst();
		// System.out.println(c.getDouble(c.getColumnIndex(LATITUDE)));
		// System.out.println(c1.getDouble(c1.getColumnIndex(LATITUDE)));
		// System.out.println(c2.getDouble(c2.getColumnIndex(LATITUDE)));
		// c.moveToLast();
		// ContentValues value = new ContentValues();
		//
		// value.put(LATITUDE, c.getDouble(c.getColumnIndex(LATITUDE)));
		// value.put(LONGITUDE, c.getDouble(c.getColumnIndex(LONGITUDE)));
		// db.getWritableDatabase().insert(TABLE_CURVE34, null, value);
		// ContentValues value = new ContentValues();
		//
		// value.put(LATITUDE, c.getDouble(c.getColumnIndex(LATITUDE)));
		// value.put(LONGITUDE, c.getDouble(c.getColumnIndex(LONGITUDE)));
		// db.getWritableDatabase().insert(TABLE_NODE, null, value);
		// Cursor c1;
		// c1 = db.getReadableDatabase().query(TABLE_NODE, null, null, null,
		// null,
		// null, null);
		// c1.moveToFirst();
		// System.out.println(c.getInt(c.getColumnIndex("_id")));
		// db.getWritableDatabase().update(TABLE_CURVE35, value, "_id " + "= 8",
		// null);
		// toast(String.valueOf(c.getCount()));
		prefs = this.getSharedPreferences(unmatched, Context.MODE_PRIVATE);

	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		db = new Database(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		db.close();

	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();

			}
		}

	}

	private void setUpMap() {
		UiSettings settings = mMap.getUiSettings();
		settings.setAllGesturesEnabled(true);
		// settings.setMyLocationButtonEnabled(true);
		mMap.setMyLocationEnabled(true);
		mMap.moveCamera(CameraUpdateFactory
				.newCameraPosition(INDIRANAGAR_position));
		mMap.setOnMapClickListener(this);
		mMap.setOnMapLongClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		item = menu.findItem(R.id.distance);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.copy:
			getSelectedCurve();
			break;
		case R.id.mcurve1:
			for (int i = 1; i <= 39; i++) {
				drawLine("TABLE_CURVE" + String.valueOf(i));
			}
			break;
		case R.id.mnode:
			markNode(TABLE_NODE);

			break;
		case R.id.clear_map:
			mMap.clear();
			break;
		case R.id.clear_curves:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Choose Curve...");
			builder.setItems(R.array.delete_array,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							db.getWritableDatabase().delete(
									"TABLE_CURVE" + String.valueOf(which + 1),
									null, null);
						}

					});
			AlertDialog alert = builder.create();
			alert.show();

			break;

		}

		return true;
	}

	private void drawLine(String tableName) {
		// mMap.clear();

		Cursor c;
		c = db.getReadableDatabase().query(tableName, null, null, null, null,
				null, null);
		PolylineOptions polylineOptions = new PolylineOptions();
		polylineOptions.color(Color.RED);
		polylineOptions.width(5);
		// arrayPoints = new ArrayList<LatLng>();
		c.moveToFirst();
		do {
			LatLng l = new LatLng(
					c.getDouble(c.getColumnIndexOrThrow(LATITUDE)),
					c.getDouble(c.getColumnIndexOrThrow(LONGITUDE)));
			polylineOptions.add(l);
			// System.out.println("tick " +l);
			// mMap.addMarker(new MarkerOptions()
			// .position(l)
			// .title("UnMatched point "));
			// mMap.clear();
			Polyline line = mMap.addPolyline(polylineOptions);
			// arrayPoints.add(l);
		} while (c.moveToNext());
		c.moveToFirst();
		calculateDistance(c, tableName);
		// if (!prefs.getBoolean(tableName + "made", false)) {
		// drawSavitzkyLine(tableName);
		// toast("drawn unmatched line");
		// prefs.edit().putBoolean(tableName + "made", true).commit();
		// } else
		// toast("drawn matched line");
		// polylineOptions.addAll(arrayPoints);

	}

	public void drawRouteOnSameCurve() {
		// mMap.clear();
		PolylineOptions polylineOptions = new PolylineOptions();
		polylineOptions.color(Color.RED);
		polylineOptions.width(5);
		Cursor c;
		c = db.getReadableDatabase().query(sourceOnCurve, null, null, null,
				null, null, null);
		c.moveToFirst();
		mMap.addMarker(new MarkerOptions().position(matchedSource).position(
				matchedDestination));
		do {
			LatLng l = new LatLng(
					c.getDouble(c.getColumnIndexOrThrow(LATITUDE)),
					c.getDouble(c.getColumnIndexOrThrow(LONGITUDE)));
			if (l.equals(matchedSource)) {
				// System.out.println("equals matched source");
				polylineOptions.add(l);
				Polyline line = mMap.addPolyline(polylineOptions);
				while (c.moveToNext()) {
					LatLng l1 = new LatLng(c.getDouble(c
							.getColumnIndexOrThrow(LATITUDE)), c.getDouble(c
							.getColumnIndexOrThrow(LONGITUDE)));
					if (l1.equals(matchedDestination)) {
						polylineOptions.add(l1);
						line = mMap.addPolyline(polylineOptions);
						return;
					} else {
						polylineOptions.add(l1);
						line = mMap.addPolyline(polylineOptions);
					}
				}
			} else if (l.equals(matchedDestination)) {
				polylineOptions.add(l);
				Polyline line = mMap.addPolyline(polylineOptions);
				while (c.moveToNext()) {
					LatLng l1 = new LatLng(c.getDouble(c
							.getColumnIndexOrThrow(LATITUDE)), c.getDouble(c
							.getColumnIndexOrThrow(LONGITUDE)));
					if (l1.equals(matchedSource)) {
						polylineOptions.add(l1);
						line = mMap.addPolyline(polylineOptions);
						return;
					} else {
						polylineOptions.add(l1);
						line = mMap.addPolyline(polylineOptions);
					}
				}
			}
		} while (c.moveToNext());
	}

	private void drawSavitzkyLine(String tableName) {
		// mMap.clear();
		Cursor c;
		c = db.getReadableDatabase().query(tableName, null, null, null, null,
				null, null);
		int getCount = c.getCount();
		PolylineOptions polylineOptions = new PolylineOptions();
		polylineOptions.color(Color.GREEN);
		polylineOptions.width(5);
		// arrayPoints = new ArrayList<LatLng>();
		c.moveToFirst();
		double[] pointX = new double[getCount];
		double[] pointY = new double[getCount];
		int i = 0;
		do {
			pointX[i] = c.getDouble(c.getColumnIndex(LATITUDE));
			pointY[i] = c.getDouble(c.getColumnIndex(LONGITUDE));
			// System.out.println("un " + pointX[i] + " " + pointY[i]);
			if (i == 0) {
				prefs.edit()
						.putLong("firstX", Double.doubleToLongBits(pointX[i]))
						.commit();
				prefs.edit()
						.putLong("firstY", Double.doubleToLongBits(pointY[i]))
						.commit();
			} else if (i == 1) {
				prefs.edit()
						.putLong("secondX", Double.doubleToLongBits(pointX[i]))
						.commit();
				prefs.edit()
						.putLong("secondY", Double.doubleToLongBits(pointY[i]))
						.commit();
			} else if (i == 2) {
				prefs.edit()
						.putLong("thirdX", Double.doubleToLongBits(pointX[i]))
						.commit();
				prefs.edit()
						.putLong("thirdY", Double.doubleToLongBits(pointY[i]))
						.commit();
			} else if (i == getCount - 1) {
				prefs.edit()
						.putLong("lastX", Double.doubleToLongBits(pointX[i]))
						.commit();
				prefs.edit()
						.putLong("lastY", Double.doubleToLongBits(pointY[i]))
						.commit();
			} else if (i == getCount - 2) {
				prefs.edit()
						.putLong("secondLastX",
								Double.doubleToLongBits(pointX[i])).commit();
				prefs.edit()
						.putLong("secondLastY",
								Double.doubleToLongBits(pointY[i])).commit();
			} else if (i == getCount - 3) {
				prefs.edit()
						.putLong("thirdLastX",
								Double.doubleToLongBits(pointX[i])).commit();
				prefs.edit()
						.putLong("thirdLastY",
								Double.doubleToLongBits(pointY[i])).commit();
			}
			i++;
		} while (c.moveToNext());

		double[] coeffs = SGFilter.computeSGCoefficients(3, 3, 4);
		SGFilter sgFilter = new SGFilter(3, 3);
		double[] smoothX = sgFilter.smooth(pointX, new double[0],
				new double[0], coeffs);
		double[] smoothY = sgFilter.smooth(pointY, new double[0],
				new double[0], coeffs);

		// Bezier b = new Bezier(points);
		// PointF[] getpoints = b.getPoints();
		// db.getWritableDatabase().execSQL(
		// "delete from " + tableName + " where _id between 4 and "
		// + String.valueOf(getCount - 4));

		db.getWritableDatabase().delete(tableName, null, null);
		for (int j = 0; j < smoothX.length; j++) {
			ContentValues value = new ContentValues();
			if (j == 0) {
				value.put(LATITUDE,
						Double.longBitsToDouble(prefs.getLong("firstX", 0)));
				value.put(LONGITUDE,
						Double.longBitsToDouble(prefs.getLong("firstY", 0)));
			} else if (j == 1) {
				value.put(LATITUDE,
						Double.longBitsToDouble(prefs.getLong("secondX", 0)));
				value.put(LONGITUDE,
						Double.longBitsToDouble(prefs.getLong("secondY", 0)));
			} else if (j == 2) {
				value.put(LATITUDE,
						Double.longBitsToDouble(prefs.getLong("thirdX", 0)));
				value.put(LONGITUDE,
						Double.longBitsToDouble(prefs.getLong("thirdY", 0)));
			} else if (j == smoothX.length - 1) {
				value.put(LATITUDE,
						Double.longBitsToDouble(prefs.getLong("lastX", 0)));
				value.put(LONGITUDE,
						Double.longBitsToDouble(prefs.getLong("lastY", 0)));
			} else if (j == smoothX.length - 2) {
				value.put(LATITUDE, Double.longBitsToDouble(prefs.getLong(
						"secondLastX", 0)));
				value.put(LONGITUDE, Double.longBitsToDouble(prefs.getLong(
						"secondLastY", 0)));
			} else if (j == smoothX.length - 3) {
				value.put(LATITUDE,
						Double.longBitsToDouble(prefs.getLong("thirdLastX", 0)));
				value.put(LONGITUDE,
						Double.longBitsToDouble(prefs.getLong("thirdLastY", 0)));
			} else {
				value.put(LATITUDE, smoothX[j]);
				value.put(LONGITUDE, smoothY[j]);
			}
			// System.out.println("matched" + smoothX[j]);
			// System.out.println("matched " + smoothY[j]);
			db.getWritableDatabase().insert(tableName, null, value);
		}
		//
		c = db.getReadableDatabase().query(tableName, null, null, null, null,
				null, null);
		c.moveToFirst();
		do {
			LatLng l = new LatLng(
					c.getDouble(c.getColumnIndexOrThrow(LATITUDE)),
					c.getDouble(c.getColumnIndexOrThrow(LONGITUDE)));
			// System.out.println(l);
			polylineOptions.add(l);
			// System.out.println("tick " +l);
			// mMap.addMarker(new MarkerOptions()
			// .position(l)
			// .title("UnMatched point "));
			// mMap.clear();
			Polyline line = mMap.addPolyline(polylineOptions);
			// arrayPoints.add(l);
		} while (c.moveToNext());
		toast("drawn matched line");
		// polylineOptions.addAll(arrayPoints);
		c.moveToFirst();
		calculateDistance(c, tableName);

	}

	private void calculateDistance(Cursor c, String tableName) {
		double distance = distanceInM(c);
		prefs.edit()
				.putLong(tableName + "distance",
						Double.doubleToLongBits(distance)).commit();
	}

	public double distanceInM(Cursor c3) {
		double R = 6378.137; // Radius of earth in KM
		double totalDistance = 0;
		if (c3.moveToFirst()) {
			do {
				double lat1 = c3.getDouble(c3.getColumnIndex(LATITUDE));
				double lon1 = c3.getDouble(c3.getColumnIndex(LONGITUDE));
				if (c3.moveToNext()) {
					double lat2 = c3.getDouble(c3.getColumnIndex(LATITUDE));
					double lon2 = c3.getDouble(c3.getColumnIndex(LONGITUDE));
					double dLat = (lat2 - lat1) * Math.PI / 180;
					double dLon = (lon2 - lon1) * Math.PI / 180;
					double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
							+ Math.cos(lat1 * Math.PI / 180)
							* Math.cos(lat2 * Math.PI / 180)
							* Math.sin(dLon / 2) * Math.sin(dLon / 2);
					double distance = (2 * Math.atan2(Math.sqrt(a),
							Math.sqrt(1 - a)))
							* R * 1000;
					totalDistance += distance;
					c3.moveToPrevious();
				} else {
					return totalDistance;
				}
			} while (c3.moveToNext());
		}
		return totalDistance;
	}

	private void markNode(String tableNode) {
		Cursor c;
		c = db.getReadableDatabase().query(tableNode, null, null, null, null,
				null, null);
		c.moveToFirst();
		for (int i = 0; i < c.getCount(); i++) {
			mMap.addMarker(new MarkerOptions().position(
					new LatLng(c.getDouble(c.getColumnIndex(LATITUDE)), c
							.getDouble(c.getColumnIndex(LONGITUDE)))).title(
					"Node " + String.valueOf(i)));
			c.moveToNext();
		}
	}

	@Override
	public void onMapClick(LatLng point) {
		lat = point.latitude;
		lon = point.longitude;
		// System.out.println(lat + " " + lon);
		getSelectedCurve();
	}

	public void getSelectedCurve() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose Curve...");
		builder.setItems(R.array.curve_array,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						insert(which);

					}

				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void insert(int index) {
		if (index == NODE) {
			ContentValues value = new ContentValues();
			value.put(LATITUDE, lat);
			value.put(LONGITUDE, lon);
			db.getWritableDatabase().insert(TABLE_NODE, null, value);
			toast("Inserted in node " + lat + " and " + lon);
		} else {
			ContentValues value = new ContentValues();
			value.put(LATITUDE, lat);
			value.put(LONGITUDE, lon);
			db.getWritableDatabase().insert(
					"TABLE_CURVE" + String.valueOf(index), null, value);
			toast("Inserted in curve " + String.valueOf(index) + lat + " and "
					+ lon);
		}

	}

	@Override
	public void onMapLongClick(LatLng point) {
		mMap.clear();
		destination = point;
		toast("Finding shortest route");
		findLocation();
	}

	public void findLocation() {
		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			if (this != null)
				toast("GPS enabled. Finding fine location. ");
			ll = new GpsListener();
			// locationManager.requestLocationUpdates(
			// LocationManager.GPS_PROVIDER, 100, 0, ll);
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 15, ll);

		} else {
			if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				if (this != null)
					toast("GPS disabled. Finding coarse location.");
				ll = new GpsListener();
				locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, ll);

			} else {
				toast("Enable location settings to get current location.");

			}
		}
	}

	class GpsListener implements LocationListener {

		public void onLocationChanged(Location location) {
			if (location != null) {
				try {
					toast("Location Found");
				} catch (Exception e) {

				}

				lat = location.getLatitude();
				lon = location.getLongitude();

				// currentLatitude = Double.toString(lat);
				// currentLongitude = Double.toString(lon);
				LatLng source = new LatLng(lat, lon);
				matchMap(source, destination);

				try {
					if (ll != null)
						locationManager.removeUpdates(ll);
				} catch (Exception e) {

				}

				locationManager = null;

			} else {
				toast("No Location Found");

			}

		}

		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

	}

	public void matchMap(LatLng source, LatLng destination) {

		matchedSource = showMatchedPoints(source, "source");
		matchedDestination = showMatchedPoints(destination, "destination");
		// Node sourceNode = getNode(matchedSource, TABLE_NODE);
		// Node destinationNode = getNode(matchedDestination, TABLE_NODE);
		Dijkstra d = new Dijkstra();
		d.executeDijkstra();

	}

	private LatLng showMatchedPoints(LatLng point, String title) {

		Cursor[] c = new Cursor[40];
		int minCurve = 0;
		min = Double.MAX_VALUE;
		c[0] = db.getReadableDatabase().query(TABLE_NODE, null, null, null,
				null, null, null);
		for (int i = 1; i <= 39; i++) {
			c[i] = db.getReadableDatabase().query(
					"TABLE_CURVE" + String.valueOf(i), null, null, null, null,
					null, null);
		}

		double distC[] = new double[39];
		for (int i = 0; i < 39; i++) {
			distC[i] = getMinimumDistanceFromCurve(point.latitude,
					point.longitude, c[i + 1]);
		}
		for (int i = 0; i < 39; i++) {
			if (distC[i] < min) {
				min = distC[i];
				minCurve = i + 1;
			}

		}
		if (title.equalsIgnoreCase("source"))
			sourceOnCurve = getCurve(minCurve);
		else if (title.equalsIgnoreCase("destination"))
			destinationOnCurve = getCurve(minCurve);
		System.out.println(sourceOnCurve + " source");
		System.out.println(destinationOnCurve + " destination");
		int minPos = 0;
		min = Double.MAX_VALUE;
		c[minCurve].moveToFirst();
		do {
			double distance = sqr(point.latitude
					- c[minCurve].getDouble(c[minCurve]
							.getColumnIndex(LATITUDE)))
					+ sqr(point.longitude
							- c[minCurve].getDouble(c[minCurve]
									.getColumnIndex(LONGITUDE)));
			if (distance < min) {
				min = distance;
				minPos = c[minCurve].getPosition();
			}
		} while (c[minCurve].moveToNext());
		c[minCurve].moveToPosition(minPos);
		LatLng markerPos = new LatLng(c[minCurve].getDouble(c[minCurve]
				.getColumnIndex(LATITUDE)), c[minCurve].getDouble(c[minCurve]
				.getColumnIndex(LONGITUDE)));
		mMap.addMarker(new MarkerOptions().position(markerPos).title(title));
		return markerPos;
	}

	private String getCurve(int value) {
		return "TABLE_CURVE" + String.valueOf(value);
	}

	public double sqr(double x) {
		return x * x;
	}

	private double getMinimumDistanceFromCurve(double pLat, double pLon,
			Cursor c1) {
		c1.moveToFirst();
		double lStartLat = c1.getDouble(c1.getColumnIndex(LATITUDE));
		double lStartLon = c1.getDouble(c1.getColumnIndex(LONGITUDE));
		c1.moveToLast();
		double lEndLat = c1.getDouble(c1.getColumnIndex(LATITUDE));
		double lEndLon = c1.getDouble(c1.getColumnIndex(LONGITUDE));

		return distToSegmentSquared(lStartLat, lStartLon, lEndLat, lEndLon,
				pLat, pLon);
	}

	public double distToSegmentSquared(double lStartLat, double lStartLon,
			double lEndLat, double lEndLon, double pLat, double pLon) {

		double diffX = lEndLat - lStartLat;
		double diffY = lEndLon - lStartLon;
		if ((diffX == 0) && (diffY == 0)) {
			diffX = pLat - lStartLat;
			diffY = pLon - lStartLon;
			return diffX * diffX + diffY * diffY;
		}

		double t = ((pLat - lStartLat) * diffX + (pLon - lStartLon) * diffY)
				/ (diffX * diffX + diffY * diffY);
		if (t < 0) {
			diffX = pLat - lStartLat;
			diffY = pLon - lStartLon;

		} else if (t > 1) {

			diffX = pLat - lEndLat;
			diffY = pLon - lEndLon;
		} else {
			// System.out.println("t in between");
			diffX = pLat - (lStartLat + t * diffX);
			diffY = pLon - (lStartLon + t * diffY);
		}
		return sqr(diffX) + sqr(diffY);
	}

	private void replaceAdjacencies(Node[] onCurveNodes, int from) {
		double dist[] = new double[2];
		int flag = 0;
		if (from == SOURCE) {
			dist = distanceInM(matchedSource, SOURCE);
			System.out.println("source " + dist[0] + " and " + dist[1]);
			for (int j = 0; j < onCurveNodes[0].adjacencies.length; j++) {
				if (onCurveNodes[0].adjacencies[j].target
						.equals(onCurveNodes[1])) {
					System.out.println("source target equal");
					onCurveNodes[0].adjacencies[j].target = n[29];
					n[29].adjacencies[0].target = onCurveNodes[0];
					boolean small = getSmallerNode(sourceOnCurve);
					if (small) {
						flag = 1;
						onCurveNodes[0].adjacencies[j].weight = dist[0];
						n[29].adjacencies[0].weight = dist[0];
						break;
					} else {
						flag = 0;
						onCurveNodes[0].adjacencies[j].weight = dist[1];
						n[29].adjacencies[0].weight = dist[1];
						break;
					}
				}
			}
			for (int j = 0; j < onCurveNodes[1].adjacencies.length; j++) {
				if (onCurveNodes[1].adjacencies[j].target
						.equals(onCurveNodes[0])) {
					onCurveNodes[1].adjacencies[j].target = n[29];
					n[29].adjacencies[1].target = onCurveNodes[1];
					if (flag == 1) {
						onCurveNodes[1].adjacencies[j].weight = dist[1];
						n[29].adjacencies[1].weight = dist[1];
						break;
					} else {
						onCurveNodes[1].adjacencies[j].weight = dist[0];
						n[29].adjacencies[1].weight = dist[0];
						break;
					}
				}
			}
		} else if (from == DESTINATION) {
			dist = distanceInM(matchedDestination, DESTINATION);
			System.out.println("destination " + dist[0] + " and " + dist[1]);
			for (int j = 0; j < onCurveNodes[0].adjacencies.length; j++) {
				if (onCurveNodes[0].adjacencies[j].target
						.equals(onCurveNodes[1])) {
					System.out.println("destination equal");
					onCurveNodes[0].adjacencies[j].target = n[30];
					n[30].adjacencies[0].target = onCurveNodes[0];
					boolean small = getSmallerNode(destinationOnCurve);
					if (small) {
						flag = 1;
						onCurveNodes[0].adjacencies[j].weight = dist[0];
						n[30].adjacencies[0].weight = dist[0];
						break;
					} else {
						flag = 0;
						onCurveNodes[0].adjacencies[j].weight = dist[1];
						n[30].adjacencies[0].weight = dist[1];
						break;
					}
				}
			}
			for (int j = 0; j < onCurveNodes[1].adjacencies.length; j++) {
				if (onCurveNodes[1].adjacencies[j].target
						.equals(onCurveNodes[0])) {
					onCurveNodes[1].adjacencies[j].target = n[30];
					n[30].adjacencies[1].target = onCurveNodes[1];
					if (flag == 1) {
						onCurveNodes[1].adjacencies[j].weight = dist[1];
						n[30].adjacencies[1].weight = dist[1];
						break;
					} else {
						onCurveNodes[1].adjacencies[j].weight = dist[0];
						n[30].adjacencies[1].weight = dist[0];
						break;
					}
				}
			}
		}

	}

	private boolean getSmallerNode(String tableName) {
		double distance1 = 0, distance2 = 0;

		Cursor c = db.getReadableDatabase().query(tableName, null, null, null,
				null, null, null);
		c.moveToFirst();
		distance1 = sqr(matchedSource.latitude
				- c.getDouble(c.getColumnIndex(LATITUDE)))
				+ sqr(matchedSource.longitude
						- c.getDouble(c.getColumnIndex(LONGITUDE)));
		c.moveToLast();
		distance2 = sqr(matchedSource.latitude
				- c.getDouble(c.getColumnIndex(LATITUDE)))
				+ sqr(matchedSource.longitude
						- c.getDouble(c.getColumnIndex(LONGITUDE)));

		if (distance1 < distance2)
			return true;
		else
			return false;
	}

	public double[] distanceInM(LatLng matchedSource2, int from) {
		Cursor c3 = null;
		if (from == SOURCE)
			c3 = db.getReadableDatabase().query(sourceOnCurve, null, null,
					null, null, null, null);
		else if (from == DESTINATION)
			c3 = db.getReadableDatabase().query(destinationOnCurve, null, null,
					null, null, null, null);
		double R = 6378.137; // Radius of earth in KM
		double totalDistance[] = new double[2];
		if (c3.moveToFirst()) {
			do {
				double lat1 = c3.getDouble(c3.getColumnIndex(LATITUDE));
				double lon1 = c3.getDouble(c3.getColumnIndex(LONGITUDE));
				if (!matchedSource2.equals(new LatLng(lat1, lon1))) {
					if (c3.moveToNext()) {
						double lat2 = c3.getDouble(c3.getColumnIndex(LATITUDE));
						double lon2 = c3
								.getDouble(c3.getColumnIndex(LONGITUDE));
						double dLat = (lat2 - lat1) * Math.PI / 180;
						double dLon = (lon2 - lon1) * Math.PI / 180;
						double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
								+ Math.cos(lat1 * Math.PI / 180)
								* Math.cos(lat2 * Math.PI / 180)
								* Math.sin(dLon / 2) * Math.sin(dLon / 2);
						double distance = (2 * Math.atan2(Math.sqrt(a),
								Math.sqrt(1 - a)))
								* R * 1000;
						totalDistance[0] += distance;
						c3.moveToPrevious();
					}
				} else {
					while (true) {
						lat1 = c3.getDouble(c3.getColumnIndex(LATITUDE));
						lon1 = c3.getDouble(c3.getColumnIndex(LONGITUDE));
						if (c3.moveToNext()) {
							double lat2 = c3.getDouble(c3
									.getColumnIndex(LATITUDE));
							double lon2 = c3.getDouble(c3
									.getColumnIndex(LONGITUDE));
							double dLat = (lat2 - lat1) * Math.PI / 180;
							double dLon = (lon2 - lon1) * Math.PI / 180;
							double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
									+ Math.cos(lat1 * Math.PI / 180)
									* Math.cos(lat2 * Math.PI / 180)
									* Math.sin(dLon / 2) * Math.sin(dLon / 2);
							double distance = (2 * Math.atan2(Math.sqrt(a),
									Math.sqrt(1 - a))) * R * 1000;
							totalDistance[1] += distance;
						} else {
							if (totalDistance[0] > totalDistance[1]) {
								double b = totalDistance[1];
								totalDistance[1] = totalDistance[0];
								totalDistance[0] = b;
							}
							return totalDistance;
						}
					}
				}
			} while (c3.moveToNext());
		}
		return totalDistance;
	}

	private Node[] getNodes(String onCurve) {
		Node[] node = new Node[2];
		if (onCurve.equalsIgnoreCase(TABLE_CURVE1)) {
			node[0] = n[0];
			node[1] = n[1];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE2)) {
			node[0] = n[1];
			node[1] = n[2];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE3)) {
			node[0] = n[1];
			node[1] = n[3];
			// System.out.println(node[0].name);

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE4)) {
			node[0] = n[3];
			node[1] = n[4];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE5)) {
			node[0] = n[4];
			node[1] = n[5];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE6)) {
			node[0] = n[5];
			node[1] = n[6];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE7)) {
			node[0] = n[6];
			node[1] = n[7];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE8)) {
			node[0] = n[2];
			node[1] = n[7];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE9)) {
			node[0] = n[0];
			node[1] = n[8];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE10)) {
			node[0] = n[3];
			node[1] = n[9];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE11)) {
			node[0] = n[4];
			node[1] = n[11];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE12)) {
			node[0] = n[5];
			node[1] = n[13];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE13)) {
			node[0] = n[6];
			node[1] = n[14];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE14)) {
			node[0] = n[7];
			node[1] = n[15];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE15)) {
			node[0] = n[8];
			node[1] = n[9];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE16)) {
			node[0] = n[9];
			node[1] = n[10];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE17)) {
			node[0] = n[10];
			node[1] = n[11];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE18)) {
			node[0] = n[11];
			node[1] = n[12];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE19)) {
			node[0] = n[12];
			node[1] = n[13];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE20)) {
			node[0] = n[13];
			node[1] = n[14];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE21)) {
			node[0] = n[14];
			node[1] = n[15];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE22)) {
			node[0] = n[8];
			node[1] = n[16];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE23)) {
			node[0] = n[16];
			node[1] = n[17];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE24)) {
			node[0] = n[12];
			node[1] = n[18];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE25)) {
			node[0] = n[16];
			node[1] = n[17];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE26)) {
			node[0] = n[17];
			node[1] = n[18];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE27)) {
			node[0] = n[18];
			node[1] = n[19];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE28)) {
			node[0] = n[19];
			node[1] = n[20];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE29)) {
			node[0] = n[20];
			node[1] = n[21];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE30)) {
			node[0] = n[16];
			node[1] = n[26];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE31)) {
			node[0] = n[17];
			node[1] = n[22];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE32)) {
			node[0] = n[19];
			node[1] = n[23];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE33)) {
			node[0] = n[20];
			node[1] = n[24];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE34)) {
			node[0] = n[22];
			node[1] = n[23];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE35)) {
			node[0] = n[23];
			node[1] = n[24];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE36)) {
			node[0] = n[24];
			node[1] = n[25];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE37)) {
			node[0] = n[22];
			node[1] = n[27];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE38)) {
			node[0] = n[26];
			node[1] = n[27];

		} else if (onCurve.equalsIgnoreCase(TABLE_CURVE39)) {
			node[0] = n[27];
			node[1] = n[28];

		}
		return node;
	}

	// private void drawRouteForSameCurve(String sourceOnCurve2) {
	// Cursor c = db.getReadableDatabase().query(sourceOnCurve2, null, null,
	// null,
	// null, null, null);
	// c.moveToFirst();string
	// do{
	//
	// }while
	// }

	public static List<Node> getShortestPathTo(Node target) {
		List<Node> path = new ArrayList<Node>();
		for (Node Node = target; Node != null; Node = Node.previous)
			path.add(Node);
		Collections.reverse(path);
		return path;
	}

	private double getCurveDistance(int i) {

		return Double.longBitsToDouble(prefs.getLong(
				"TABLE_CURVE" + String.valueOf(i) + "distance", 0));

	}

	private void toast(String string) {
		Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT)
				.show();

	}

	public class Dijkstra {
		public void computePaths(Node source) {
			source.minDistance = 0.;
			PriorityQueue<Node> vertexQueue = new PriorityQueue<Node>();
			vertexQueue.add(source);

			while (!vertexQueue.isEmpty()) {
				Node u = vertexQueue.poll();

				// Visit each edge exiting u
				for (Edge e : u.adjacencies) {
					// System.out.println(u.adjacencies);
					Node v = e.target;
					double weight = e.weight;
					double distanceThroughU = u.minDistance + weight;
					if (distanceThroughU < v.minDistance) {
						vertexQueue.remove(v);
						v.minDistance = distanceThroughU;
						v.previous = u;
						vertexQueue.add(v);
					}
				}
			}
		}

		public void executeDijkstra() {
			n = new Node[31];
			for (int i = 0; i < 31; i++) {
				n[i] = new Node(String.valueOf(i));
			}

			n[0].adjacencies = new Edge[] {
					new Edge(n[1], getCurveDistance(1)),
					new Edge(n[8], getCurveDistance(9)) };
			n[1].adjacencies = new Edge[] {
					new Edge(n[0], getCurveDistance(1)),
					new Edge(n[2], getCurveDistance(2)),
					new Edge(n[3], getCurveDistance(3)) };
			n[2].adjacencies = new Edge[] {
					new Edge(n[1], getCurveDistance(2)),
					new Edge(n[7], getCurveDistance(8)) };
			n[3].adjacencies = new Edge[] {
					new Edge(n[1], getCurveDistance(3)),
					new Edge(n[4], getCurveDistance(4)),
					new Edge(n[9], getCurveDistance(10)) };
			n[4].adjacencies = new Edge[] {
					new Edge(n[3], getCurveDistance(4)),
					new Edge(n[5], getCurveDistance(5)),
					new Edge(n[11], getCurveDistance(11)) };
			n[5].adjacencies = new Edge[] {
					new Edge(n[4], getCurveDistance(5)),
					new Edge(n[6], getCurveDistance(6)),
					new Edge(n[13], getCurveDistance(12)) };
			n[6].adjacencies = new Edge[] {
					new Edge(n[5], getCurveDistance(6)),
					new Edge(n[7], getCurveDistance(7)),
					new Edge(n[14], getCurveDistance(13)) };
			n[7].adjacencies = new Edge[] {
					new Edge(n[2], getCurveDistance(8)),
					new Edge(n[6], getCurveDistance(7)),
					new Edge(n[15], getCurveDistance(14)) };
			n[8].adjacencies = new Edge[] {
					new Edge(n[0], getCurveDistance(9)),
					new Edge(n[9], getCurveDistance(15)),
					new Edge(n[16], getCurveDistance(22)) };
			n[9].adjacencies = new Edge[] {
					new Edge(n[3], getCurveDistance(10)),
					new Edge(n[8], getCurveDistance(15)),
					new Edge(n[10], getCurveDistance(16)) };
			n[10].adjacencies = new Edge[] {
					new Edge(n[9], getCurveDistance(16)),
					new Edge(n[11], getCurveDistance(17)),
					new Edge(n[17], getCurveDistance(23)) };
			n[11].adjacencies = new Edge[] {
					new Edge(n[10], getCurveDistance(17)),
					new Edge(n[12], getCurveDistance(18)),
					new Edge(n[4], getCurveDistance(11)) };
			n[12].adjacencies = new Edge[] {
					new Edge(n[11], getCurveDistance(18)),
					new Edge(n[13], getCurveDistance(19)),
					new Edge(n[18], getCurveDistance(24)) };
			n[13].adjacencies = new Edge[] {
					new Edge(n[12], getCurveDistance(19)),
					new Edge(n[14], getCurveDistance(20)),
					new Edge(n[5], getCurveDistance(12)) };
			n[15].adjacencies = new Edge[] {
					new Edge(n[14], getCurveDistance(21)),
					new Edge(n[7], getCurveDistance(14)) };
			n[16].adjacencies = new Edge[] {
					new Edge(n[8], getCurveDistance(22)),
					new Edge(n[17], getCurveDistance(25)),
					new Edge(n[26], getCurveDistance(30)) };
			n[17].adjacencies = new Edge[] {
					new Edge(n[10], getCurveDistance(23)),
					new Edge(n[16], getCurveDistance(25)),
					new Edge(n[18], getCurveDistance(26)),
					new Edge(n[22], getCurveDistance(31)) };
			n[18].adjacencies = new Edge[] {
					new Edge(n[12], getCurveDistance(24)),
					new Edge(n[17], getCurveDistance(26)),
					new Edge(n[19], getCurveDistance(27)) };
			n[19].adjacencies = new Edge[] {
					new Edge(n[18], getCurveDistance(27)),
					new Edge(n[20], getCurveDistance(28)),
					new Edge(n[23], getCurveDistance(32)) };
			n[20].adjacencies = new Edge[] {
					new Edge(n[19], getCurveDistance(28)),
					new Edge(n[21], getCurveDistance(29)),
					new Edge(n[24], getCurveDistance(33)) };
			n[21].adjacencies = new Edge[] { new Edge(n[20],
					getCurveDistance(29)) };
			n[22].adjacencies = new Edge[] {
					new Edge(n[17], getCurveDistance(31)),
					new Edge(n[23], getCurveDistance(34)),
					new Edge(n[27], getCurveDistance(37)) };
			n[23].adjacencies = new Edge[] {
					new Edge(n[19], getCurveDistance(32)),
					new Edge(n[22], getCurveDistance(34)),
					new Edge(n[24], getCurveDistance(35)) };
			n[24].adjacencies = new Edge[] {
					new Edge(n[20], getCurveDistance(33)),
					new Edge(n[23], getCurveDistance(35)),
					new Edge(n[25], getCurveDistance(36)) };
			n[25].adjacencies = new Edge[] { new Edge(n[24],
					getCurveDistance(36)) };
			n[26].adjacencies = new Edge[] {
					new Edge(n[16], getCurveDistance(30)),
					new Edge(n[27], getCurveDistance(38)) };
			n[27].adjacencies = new Edge[] {
					new Edge(n[22], getCurveDistance(37)),
					new Edge(n[26], getCurveDistance(38)),
					new Edge(n[28], getCurveDistance(39)) };

			n[28].adjacencies = new Edge[] { new Edge(n[27],
					getCurveDistance(39)) };

			n[29].adjacencies = new Edge[] { new Edge(n[3], 0),
					new Edge(n[1], 1) };// dummy values just to initialize node
			n[30].adjacencies = new Edge[] { new Edge(n[3], 0),
					new Edge(n[1], 1) };// dummy values

			if (sourceOnCurve.equalsIgnoreCase(destinationOnCurve)) {
				toast("Your source and destination is on same curve!");
				drawRouteOnSameCurve();
			} else {
				// System.out.println("Not same ");
				Node[] sourceOnCurveNodes = getNodes(sourceOnCurve);
				replaceAdjacencies(sourceOnCurveNodes, SOURCE);
				Node[] destinationOnCurveNodes = getNodes(destinationOnCurve);
				replaceAdjacencies(destinationOnCurveNodes, DESTINATION);
				// System.out.println("length "+n[8].adjacencies.length+" target "+n[8].adjacencies[0].target
				// +" weight "+n[8].adjacencies[0].weight);
				// System.out.println("length "+n[8].adjacencies.length+" target "+n[8].adjacencies[1].target
				// +" weight "+n[8].adjacencies[1].weight);
				// System.out.println("length "+n[9].adjacencies.length+" target "+n[9].adjacencies[0].target
				// +" weight "+n[9].adjacencies[0].weight);
				// System.out.println("length "+n[9].adjacencies.length+" target "+n[9].adjacencies[1].target
				// +" weight "+n[9].adjacencies[1].weight);
				computePaths(n[29]);
				// for (Node destiNode : n) {
				System.out.println("Distance to Destination : "
						+ n[30].minDistance);
				item.setVisible(true);
				item.setTitle(String.format("%.2f", n[30].minDistance)
						+ " mtrs");
				List<Node> path = getShortestPathTo(n[30]);
				System.out.println("Path: " + path);
				drawShortestRoute(path);
			}

		}
	}

	public void drawShortestRoute(List<Node> path) {
		drawTwoNodes(path, SOURCE);
		drawTwoNodes(path, DESTINATION);
		if (path.size() == 3)
			return;
		for (int i = 1; i < path.size() - 2; i++) {
			drawLine(getCurveByNode(path.get(i), path.get(i + 1)));
		}
	}

	private void drawTwoNodes(List<Node> path, int from) {
		PolylineOptions polylineOptions = new PolylineOptions();
		polylineOptions.color(Color.RED);
		polylineOptions.width(5);
		Cursor c = null, c1 = null;
		LatLng matched = null, nodeLocation = null;
		String nextOrPreviousCurve = null;
		if (from == SOURCE) {
			matched = matchedSource;
			c = db.getReadableDatabase().query(sourceOnCurve, null, null, null,
					null, null, null);
			nextOrPreviousCurve = getCurveByNode(path.get(1), path.get(2));

		} else {
			matched = matchedDestination;
			c = db.getReadableDatabase().query(destinationOnCurve, null, null,
					null, null, null, null);
			nextOrPreviousCurve = getCurveByNode(path.get(path.size() - 3),
					path.get(path.size() - 2));
		}
		c1 = db.getReadableDatabase().query(nextOrPreviousCurve, null, null,
				null, null, null, null);
		c.moveToFirst();
		c1.moveToFirst();
		// here we find the two equal nodes of the two adjacent curves
		if (getLatlng(c).equals(getLatlng(c1)))
			nodeLocation = getLatlng(c);
		else {
			c1.moveToLast();
			if (getLatlng(c).equals(getLatlng(c1)))
				nodeLocation = getLatlng(c);
			else {
				c.moveToLast();
				if (getLatlng(c).equals(getLatlng(c1)))
					nodeLocation = getLatlng(c);
				else {
					c1.moveToFirst();
					nodeLocation = getLatlng(c1);
				}
			}
		}

		LatLng location = null;
		int position = 0;
		c.moveToFirst();
		List<LatLng> list = new ArrayList<LatLng>();
		list.add(matched);
		do {
			LatLng toMatch = new LatLng(
					c.getDouble(c.getColumnIndex(LATITUDE)), c.getDouble(c
							.getColumnIndex(LONGITUDE)));
			if (toMatch.equals(matched)) {
				position = c.getPosition();
				break;
			}
		} while (c.moveToNext());
		c.moveToPosition(position);
		while (c.moveToNext()) {
			location = new LatLng(c.getDouble(c.getColumnIndex(LATITUDE)),
					c.getDouble(c.getColumnIndex(LONGITUDE)));
			list.add(location);
			if (location.equals(nodeLocation)) {
				polylineOptions.addAll(list);
				Polyline line = mMap.addPolyline(polylineOptions);
				return;
			}
		}
		c.moveToPosition(position);
		list.clear();
		list.add(matched);
		while (c.moveToPrevious()) {
			location = new LatLng(c.getDouble(c.getColumnIndex(LATITUDE)),
					c.getDouble(c.getColumnIndex(LONGITUDE)));
			list.add(location);
			if (location.equals(nodeLocation)) {
				polylineOptions.addAll(list);
				Polyline line = mMap.addPolyline(polylineOptions);
				return;
			}
		}
	}

	public String getCurveByNode(Node n1, Node n2) {
		if ((n1.equals(n[0]) && n2.equals(n[1]))
				|| (n1.equals(n[1]) && n2.equals(n[0])))
			return TABLE_CURVE1;
		else if ((n1.equals(n[1]) && n2.equals(n[2]))
				|| (n1.equals(n[2]) && n2.equals(n[1])))
			return TABLE_CURVE2;
		else if ((n1.equals(n[1]) && n2.equals(n[3]))
				|| (n1.equals(n[3]) && n2.equals(n[1])))
			return TABLE_CURVE3;
		else if ((n1.equals(n[3]) && n2.equals(n[4]))
				|| (n1.equals(n[4]) && n2.equals(n[3])))
			return TABLE_CURVE4;
		else if ((n1.equals(n[4]) && n2.equals(n[5]))
				|| (n1.equals(n[5]) && n2.equals(n[4])))
			return TABLE_CURVE5;
		else if ((n1.equals(n[5]) && n2.equals(n[6]))
				|| (n1.equals(n[6]) && n2.equals(n[5])))
			return TABLE_CURVE6;
		else if ((n1.equals(n[7]) && n2.equals(n[6]))
				|| (n1.equals(n[6]) && n2.equals(n[7])))
			return TABLE_CURVE7;
		else if ((n1.equals(n[2]) && n2.equals(n[7]))
				|| (n1.equals(n[7]) && n2.equals(n[2])))
			return TABLE_CURVE8;
		else if ((n1.equals(n[0]) && n2.equals(n[8]))
				|| (n1.equals(n[8]) && n2.equals(n[0])))
			return TABLE_CURVE9;
		else if ((n1.equals(n[3]) && n2.equals(n[9]))
				|| (n1.equals(n[9]) && n2.equals(n[3])))
			return TABLE_CURVE10;
		else if ((n1.equals(n[4]) && n2.equals(n[11]))
				|| (n1.equals(n[11]) && n2.equals(n[4])))
			return TABLE_CURVE11;
		else if ((n1.equals(n[5]) && n2.equals(n[13]))
				|| (n1.equals(n[13]) && n2.equals(n[5])))
			return TABLE_CURVE12;
		else if ((n1.equals(n[6]) && n2.equals(n[14]))
				|| (n1.equals(n[14]) && n2.equals(n[6])))
			return TABLE_CURVE13;
		else if ((n1.equals(n[7]) && n2.equals(n[15]))
				|| (n1.equals(n[15]) && n2.equals(n[7])))
			return TABLE_CURVE14;
		else if ((n1.equals(n[8]) && n2.equals(n[9]))
				|| (n1.equals(n[9]) && n2.equals(n[8])))
			return TABLE_CURVE15;
		else if ((n1.equals(n[9]) && n2.equals(n[10]))
				|| (n1.equals(n[10]) && n2.equals(n[9])))
			return TABLE_CURVE16;
		else if ((n1.equals(n[10]) && n2.equals(n[11]))
				|| (n1.equals(n[11]) && n2.equals(n[10])))
			return TABLE_CURVE17;
		else if ((n1.equals(n[11]) && n2.equals(n[12]))
				|| (n1.equals(n[12]) && n2.equals(n[11])))
			return TABLE_CURVE18;
		else if ((n1.equals(n[12]) && n2.equals(n[13]))
				|| (n1.equals(n[13]) && n2.equals(n[12])))
			return TABLE_CURVE19;
		else if ((n1.equals(n[13]) && n2.equals(n[14]))
				|| (n1.equals(n[14]) && n2.equals(n[13])))
			return TABLE_CURVE20;
		else if ((n1.equals(n[14]) && n2.equals(n[15]))
				|| (n1.equals(n[15]) && n2.equals(n[14])))
			return TABLE_CURVE21;
		else if ((n1.equals(n[8]) && n2.equals(n[16]))
				|| (n1.equals(n[16]) && n2.equals(n[8])))
			return TABLE_CURVE22;
		else if ((n1.equals(n[10]) && n2.equals(n[17]))
				|| (n1.equals(n[17]) && n2.equals(n[10])))
			return TABLE_CURVE23;
		else if ((n1.equals(n[12]) && n2.equals(n[18]))
				|| (n1.equals(n[18]) && n2.equals(n[12])))
			return TABLE_CURVE24;
		else if ((n1.equals(n[16]) && n2.equals(n[17]))
				|| (n1.equals(n[17]) && n2.equals(n[16])))
			return TABLE_CURVE25;
		else if ((n1.equals(n[17]) && n2.equals(n[18]))
				|| (n1.equals(n[18]) && n2.equals(n[17])))
			return TABLE_CURVE26;
		else if ((n1.equals(n[18]) && n2.equals(n[19]))
				|| (n1.equals(n[19]) && n2.equals(n[18])))
			return TABLE_CURVE27;
		else if ((n1.equals(n[19]) && n2.equals(n[20]))
				|| (n1.equals(n[20]) && n2.equals(n[19])))
			return TABLE_CURVE28;
		else if ((n1.equals(n[20]) && n2.equals(n[21]))
				|| (n1.equals(n[21]) && n2.equals(n[20])))
			return TABLE_CURVE29;
		else if ((n1.equals(n[16]) && n2.equals(n[26]))
				|| (n1.equals(n[26]) && n2.equals(n[16])))
			return TABLE_CURVE30;
		else if ((n1.equals(n[17]) && n2.equals(n[22]))
				|| (n1.equals(n[22]) && n2.equals(n[17])))
			return TABLE_CURVE31;
		else if ((n1.equals(n[19]) && n2.equals(n[23]))
				|| (n1.equals(n[23]) && n2.equals(n[19])))
			return TABLE_CURVE32;
		else if ((n1.equals(n[20]) && n2.equals(n[24]))
				|| (n1.equals(n[24]) && n2.equals(n[20])))
			return TABLE_CURVE33;
		else if ((n1.equals(n[22]) && n2.equals(n[23]))
				|| (n1.equals(n[23]) && n2.equals(n[22])))
			return TABLE_CURVE34;
		else if ((n1.equals(n[23]) && n2.equals(n[24]))
				|| (n1.equals(n[24]) && n2.equals(n[23])))
			return TABLE_CURVE35;
		else if ((n1.equals(n[24]) && n2.equals(n[25]))
				|| (n1.equals(n[25]) && n2.equals(n[24])))
			return TABLE_CURVE36;
		else if ((n1.equals(n[22]) && n2.equals(n[22]))
				|| (n1.equals(n[27]) && n2.equals(n[27])))
			return TABLE_CURVE37;
		else if ((n1.equals(n[26]) && n2.equals(n[26]))
				|| (n1.equals(n[27]) && n2.equals(n[27])))
			return TABLE_CURVE38;
		else if ((n1.equals(n[27]) && n2.equals(n[28]))
				|| (n1.equals(n[28]) && n2.equals(n[27])))
			return TABLE_CURVE39;
		return "";
	}

	private LatLng getLatlng(Cursor c) {
		return new LatLng(c.getDouble(c.getColumnIndex(LATITUDE)),
				c.getDouble(c.getColumnIndex(LONGITUDE)));

	}
}
